
public class AreaVolumeConeSolver {

	/**
	 * @param args
	 */
		public static void main(String[] args) {
			
				
			if(args.length!=3) {
				System.err.print ("AreaVolumeConeSolver <r> <s> <h>");
			}
			else {
					
				double r = Double.parseDouble(args[0]);
				double s = Double.parseDouble(args[1]);
				double h = Double.parseDouble(args[2]);
				System.out.println ("For cone with r " + r + " s " + s + " h " + h);
				System.out.print ("Surface area is " + (Math.PI*r*s + Math.PI*Math.pow(r, 2)));
				System.out.println (" Volume is " + Math.PI * Math.pow(r, 2)*h/3);
			}
		}
}
